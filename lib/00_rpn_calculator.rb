class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []
    @length = 0
  end

  def push(num)
    @stack << num
    @length += 1
  end

  def plus
    self.checker
    popped1 = @stack.pop
    popped2 = @stack.pop
    @stack << popped1 + popped2
    @length -= 1
  end

  def value
    @stack[-1]
  end


  def minus
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << pen_ult_num - ultimate_num
    @length -= 1
  end

  def divide
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << pen_ult_num.fdiv(ultimate_num)
    @length -= 1
  end

  def times
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << ultimate_num * pen_ult_num
    @length -= 1
  end

    def checker
      if @length <= 1
        raise "calculator is empty"
      end
    end

end
